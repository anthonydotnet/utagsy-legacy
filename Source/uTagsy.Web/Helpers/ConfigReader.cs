﻿namespace uTagsy.Web.Helpers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;

    public class ConfigReader
    {
        private const string m_ConfigPath = "~/config/uTagsy.config";

        #region Singleton

        protected XDocument m_Doc;
        protected static volatile ConfigReader m_Instance = new ConfigReader();
        protected static object syncRoot = new Object();

        protected ConfigReader()
        {
            m_Doc = XDocument.Parse(File.ReadAllText(HttpContext.Current.Server.MapPath(m_ConfigPath)));
        }

        public static ConfigReader Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    lock (syncRoot)
                    {
                        if (m_Instance == null)
                            m_Instance = new ConfigReader();
                    }
                }


                return m_Instance;
            }
        }

        #endregion


        /// <summary>
        /// Gets aliases element
        /// </summary>
        /// <returns></returns>
        private XElement GetAliases()
        {
            return this.m_Doc.Descendants("Aliases").Single();
        }



        /// <summary>
        /// Gets RootNodeTypeAlias.
        /// </summary>
        /// <returns></returns>
        public string GetRootNodeTypeAlias()
        {
            var value = GetAliases().Descendants("RootNodeTypeAlias").Single().Value.Trim();

            return value;
        }


        /// <summary>
        /// Gets TagContainerNodeTypeAlias.
        /// </summary>
        /// <returns></returns>
        public string GetTagContainerNodeTypeAlias()
        {
            var value = GetAliases().Descendants("TagContainerNodeTypeAlias").Single().Value.Trim();

            return value;
        }

        /// <summary>
        /// Gets TagNodeTypeAlias.
        /// </summary>
        /// <returns></returns>
        public string GetTagNodeTypeAlias()
        {
            var value = GetAliases().Descendants("TagNodeTypeAlias").Single().Value.Trim();

            return value;
        }

        /// <summary>
        /// Gets TagNamePropertyAlias.
        /// </summary>
        /// <returns></returns>
        public string GetTagNamePropertyAlias()
        {
            var value = GetAliases().Descendants("TagNamePropertyAlias").Single().Value.Trim();

            return value;
        }

    }
}
